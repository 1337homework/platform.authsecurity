module.exports = {
  generalConfig: require('./general'),
  constants: require('./constants'),
  database: require('./database'),
  rabbit: require('./rabbit'),
  routings: require('./routings')
}
