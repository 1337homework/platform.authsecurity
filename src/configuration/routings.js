module.exports = {
  krakenBalance: {
    routingKey: 'kraken.balance',
    updateIntervalMinutes: 1
  },
  binanceBalance: {
    routingKey: 'binance.balance',
    updateIntervalMinutes: 1
  }
}
