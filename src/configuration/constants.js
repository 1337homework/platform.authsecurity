module.exports = {
  reasonCodes: {
    RECEIVED_INVALID_MESSAGE: 'ReceivedInvalidMessage',
    DATABASE_ERROR: 'DatabaseError',
    RABBIT_MQ_ERROR: 'RabbitMQError',
    SERVER_ERROR: 'ServerError',
    INCONSISTENT_STATE_ERROR: 'InconsistentStateError'
  },

  communicationTopology: {
    EXCHANGE_AUTH_ATTEMPTED: 'AuthAttempted',
    EXCHANGE_AUTH_VERIFIED: 'AuthVerified',
    EXCHANGE_VALIDATE_API_CALL_REQ: 'ValidateAPICallRequest',
    EXCHANGE_VALIDATE_API_CALL_RES: 'ValidateAPICallResponse',
    QUEUE_VALIDATE_API_CALL_REQ: 'ValidateApiCall_Queue',
    // QUEUE_VALIDATE_API_CALL_RES: 'ValidateApiCall_Queue_Response',
    QUEUE_API_CALL_VERIFIED: 'VerifiedApiCall_Queue'
  },

  security: {
    AUTH_VERIFIED_STATUS: 'OK',
    AUTH_FAILED_STATUS: 'Fail',
    ANONYMOUS_SCOPE: 'anonymous',
    JWT_PUBLIC_KEY_PATH: '/certs/jwtRS256.key.pub'
  },
  jwt: {
    issuer: 'aggregation-platform',
    audience: 'aggregation-platform',
    subject: 'aggregation-platform',
    expiresIn: '12h',
    algorithm: 'RS256'
  }
}
