exports.seed = function(knex) {

  const paths = [
    { id: 1, path: '/api/ka', data: JSON.stringify({ enabled: true, scopes: [ 'anonymous' ] }) },
    { id: 2, path: '/api/login', data: JSON.stringify({ enabled: true, scopes: [ 'anonymous' ] }) },
    { id: 3, path: '/api/saga', data: JSON.stringify({ enabled: true, scopes: [ 'user' ] }) },
    { id: 4, path: '/api/saga?sagaId=#', data: JSON.stringify({ enabled: true, scopes: [ 'user' ] }) },
    { id: 5, path: '/api/profile', data: JSON.stringify({ enabled: false, scopes: [ 'admin' ] }) },
    { id: 6, path: '/api/aggregate', data: JSON.stringify({ enabled: true, scopes: [ 'user' ] }) }
  ]

  return knex('security_path_entities')
    .del()
    .then(() => {
      return knex('security_path_entities').insert(paths)
    })
}
