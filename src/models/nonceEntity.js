const { Model } = require('objection')

class NonceEntity extends Model {
  static get tableName() {
    return 'nonce_entities'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['nonce', 'path', 'requestedScopes'],

      properties: {
        id: { type: 'integer' },
        nonce: { type: 'string', minLength: 1, maxLength: 255  },
        path: { type: 'string', minLength: 1, maxLength: 255 },
        requestedScopes: {
          type: 'array',
          items: { type: 'string', minLength: 1, maxLength: 255 }
        },
        createdAt: { type: 'string', minLength: 1, maxLength: 255 }
      }
    }
  }
}

module.exports = NonceEntity
