const { Model } = require('objection')

class SecurityPathEntity extends Model {
  static get tableName() {
    return 'security_path_entities'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['path'],

      properties: {
        id: { type: 'integer' },
        path: { type: 'string', minLength: 1, maxLength: 255 },
        data: {
          type: 'object',
          properties: {
            enabled: { type: 'boolean' },
            scopes: {
              type: 'array',
              items: { type: 'string', minLength: 1, maxLength: 255 }
            }
          }
        }
      }
    }
  }
}

module.exports = SecurityPathEntity
