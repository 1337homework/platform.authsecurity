const jwtHandler = require('../../jwt/jwtHandler')
const rabbit = require('../../interfaces/rabbit')
const { logger } = require('../../utils/logger')
const { getNonceEntity } = require('../../repository/nonce')
const {
  AUTH_VERIFIED_STATUS,
  AUTH_FAILED_STATUS
} = require('../../configuration/constants').security

// TODO: implement error handler to notify error upstream
const authValidationHandler = async (originalMessage) => {
  let verifiedMessage

  logger.log({ level: 'info', message: 'Got auth verified message, performing auth validation.' })

  try {
    verifiedMessage = JSON.parse(originalMessage.content.toString('utf8'))
  } catch (e) {
    logger.log({ level: 'error', message: 'Malformed auth message.' })
    rabbit.publishToAPICallResponseExchange({
      authStatus: AUTH_FAILED_STATUS
    }, originalMessage.fields.routingKey)
    return
  }

  let decodedPayload
  let payloadNonce
  try {
    decodedPayload = jwtHandler.tryDecodeJWT(verifiedMessage.authToken)
    if (verifiedMessage && verifiedMessage.nonce) {
      payloadNonce = verifiedMessage.nonce.nonce
    } else {
      payloadNonce = decodedPayload.nonce.nonce
    }
  } catch (e) {
    logger.log({ level: 'error', message: `Invalid JWT provided by auth attempt serice. ${e}` })
    rabbit.publishToAPICallResponseExchange({
      authStatus: AUTH_FAILED_STATUS
    }, originalMessage.fields.routingKey)
    rabbit.ack(originalMessage)
    return
  }
  console.log(decodedPayload)
  console.log(payloadNonce)
  // todo: change key to nonceEntity
  const nonceData = await getNonceEntity(payloadNonce)

  // should validate against expiration time also
  if (!nonceData) {
    logger.log({ level: 'error', message: 'Request expired.' })
    // handle error report to error queue
    rabbit.publishToAPICallResponseExchange({
      authStatus: AUTH_FAILED_STATUS
    }, originalMessage.fields.routingKey)
    rabbit.ack(originalMessage)
    return
  }

  // in real world this of course would be much more complex including audience
  const scopesAreRight = nonceData.requestedScopes.sort()
    .every((scope) => decodedPayload.scopes.sort().includes(scope))
  if (!scopesAreRight) {
    logger.log({
      level: 'error',
      message: `Nonce scopes ${nonceData.requestedScopes}, session scopes: ${decodedPayload.scopes}`
    })
    rabbit.publishToAPICallResponseExchange({
      authStatus: AUTH_FAILED_STATUS
    }, originalMessage.fields.routingKey)
    rabbit.ack(originalMessage)
    return
  }

  logger.log({ level: 'info', message: 'Auth is valid, publishing to next listener.' })

  rabbit.publishToAPICallResponseExchange({
    authStatus: AUTH_VERIFIED_STATUS,
    authToken: verifiedMessage.authToken
  }, originalMessage.fields.routingKey)
  rabbit.ack(originalMessage)
}

module.exports = {
  authValidationHandler
}
