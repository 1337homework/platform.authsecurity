const rabbit = require('../../interfaces/rabbit')
const { logger } = require('../../utils/logger')
const uuid = require('uuid/v4')
const { createNonceEntity } = require('../../repository/nonce')
const { getPathEntity } = require('../../repository/securityPaths')

const validateApiCallHandler = async (originalMessage) => {
  let validationMessage

  logger.log({ level: 'info', message: 'Got request validation message, starting auth and validation.' })

  try {
    validationMessage = JSON.parse(originalMessage.content.toString('utf8'))
  } catch (e) {
    logger.log({ level: 'error', message: 'Malformed auth message.' })
    // handle error report to error queue
    return
  }

  if (!validationMessage.path) {
    logger.log({ level: 'error', message: 'Path is required to provide in order to validate request.' })
    // handle error report to error queue
    rabbit.ack(originalMessage)
    return
  }

  const pathData = await getPathEntity(validationMessage.path)

  if (
    !pathData ||
    !pathData.data.scopes ||
    !pathData.data.enabled
  ) {
    logger.log({ level: 'error', message: 'Validation failed, path does not exits or is not enabled.' })
    // should handle error
    rabbit.ack(originalMessage)
    return
  }

  const nonce = await createNonceEntity(pathData.data, uuid(), validationMessage.path)

  logger.log({ level: 'info', message: 'Publishing validation request message to next listener - auth service.' })

  // try to authenticate
  rabbit.publishToAuthAttemptedExchange({
    logonPackage: validationMessage.logonPackage,
    requiredScopes: pathData.data.scopes,
    authToken: validationMessage.authToken,
    nonce,
    validationMessage
  }, originalMessage.fields.routingKey)
  rabbit.ack(originalMessage)
  return
}

module.exports = {
  validateApiCallHandler
}
