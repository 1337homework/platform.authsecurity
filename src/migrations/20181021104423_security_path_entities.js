
const schema = (t) => {
  t.increments('id').primary()
  t.string('path', 256)
  t.json('data').nullable()
}

exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('security_path_entities', (table) => schema(table, knex))
  ])
}

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('security_path_entities')
}
