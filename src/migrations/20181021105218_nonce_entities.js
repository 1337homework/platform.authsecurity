
const schema = (t, knex) => {
  t.increments('id').primary()
  t.string('nonce', 256)
  t.string('path', 256)
  t.json('requestedScopes').nullable(),
  t.datetime('createdAt', 6).defaultTo(knex.fn.now(6))
}

exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('nonce_entities', (table) => schema(table, knex))
  ])
}

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('nonce_entities')
}
