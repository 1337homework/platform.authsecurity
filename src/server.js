require('./utils/env')
const rabbitmq = require('./interfaces/rabbit')
const db = require('./interfaces/sql')

const { validateApiCallHandler } = require('./domain/services/validateAPICallService')
const { authValidationHandler } = require('./domain/services/authValidationService')
const { logger } = require('./utils/logger')

Promise.all([rabbitmq.openConnection(), db.openConnection()])
  .then(() => {
    rabbitmq.registerValidateAPICallConsumer(validateApiCallHandler)
    rabbitmq.registerAuthVerificationConsumer(authValidationHandler)
    logger.log({
      level: 'info',
      message: 'Auth Security MS Connected to RabbitMQ. Listening for commands...'
    })
  })
  .catch((err) => {
    logger.log({ level: 'error', message: `${err.message} - ${err.stack}` })
    process.exit(1)
  })
