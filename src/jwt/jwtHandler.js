const jwt = require('jsonwebtoken')
const jwtGlobalOpts = require('../configuration/constants').jwt

const tryDecodeJWT = (token, options) => {
  if (!token) {
    return new Error('Token Not provided')
  }

  const jwtOptions = {
    ...jwtGlobalOpts,
    ...options
  }

  return jwt.decode(token, jwtOptions)
}

module.exports = {
  tryDecodeJWT
}
