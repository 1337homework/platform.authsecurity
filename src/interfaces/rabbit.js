const amqp = require('amqplib')
const { rabbit } = require('../configuration')
const {
  EXCHANGE_AUTH_ATTEMPTED,
  EXCHANGE_AUTH_VERIFIED,
  EXCHANGE_VALIDATE_API_CALL_REQ,
  EXCHANGE_VALIDATE_API_CALL_RES,
  // QUEUE_VALIDATE_API_CALL_RES,
  QUEUE_VALIDATE_API_CALL_REQ,
  QUEUE_API_CALL_VERIFIED
} = require('../configuration/constants').communicationTopology
const { logger } = require('../utils/logger')
let rabbitChannel = null

const assertAuthAttemptChannelState = () => {
  return rabbitChannel.assertExchange(EXCHANGE_VALIDATE_API_CALL_REQ, 'topic', { durable: true })
    .then(() => {
      return rabbitChannel.assertQueue(QUEUE_VALIDATE_API_CALL_REQ, { durable: true })
    })
    .then((result) => {
      return rabbitChannel.bindQueue(result.queue, EXCHANGE_VALIDATE_API_CALL_REQ, '#')
    })
}

const assertAuthVerifyChannelState = () => {
  return rabbitChannel.assertExchange(EXCHANGE_AUTH_VERIFIED, 'topic', { durable: true })
    .then(() => {
      return rabbitChannel.assertQueue(QUEUE_API_CALL_VERIFIED, { durable: true })
    })
    .then((result) => {
      return rabbitChannel.bindQueue(result.queue, EXCHANGE_AUTH_VERIFIED, '#')
    })
}

const assertApiCallVerifiedResponseChannelState = () => {
  return rabbitChannel.assertExchange(EXCHANGE_VALIDATE_API_CALL_RES, 'topic', { durable: true })
}

const openConnection = () => {
  return amqp
    .connect(rabbit.connStr)
    .then((conn) => {
      return conn.createChannel()
    })
    .then((channel) => {
      rabbitChannel = channel
      return Promise.all([
        assertAuthAttemptChannelState(),
        assertAuthVerifyChannelState(),
        assertApiCallVerifiedResponseChannelState()
      ])
    })
    .catch((err) => {
      logger.log({
        level: 'error',
        message: `Failed connect to RabbitMQ, reconnecting... ${JSON.stringify(err)}`
      })
      return new Promise((resolve) => {
        setTimeout(() => resolve(openConnection()), rabbit.retryInterval)
      })
    })
}

const registerConsumer = (queue, consumer) => {
  return rabbitChannel.consume(queue, consumer)
}

const registerAuthVerificationConsumer = (consumer) => {
  return registerConsumer(QUEUE_API_CALL_VERIFIED, consumer)
}

const registerValidateAPICallConsumer = (consumer) => {
  return registerConsumer(QUEUE_VALIDATE_API_CALL_REQ, consumer)
}

const publishToExchange = (exchange, msg, routingKey) => {
  return rabbitChannel.publish(
    exchange,
    routingKey,
    Buffer.from(JSON.stringify(msg)),
    {
      persistent: true,
      contentType: 'application/json'
    }
  )
}

const publishToAuthAttemptedExchange = (msg, routingKey) => {
  return publishToExchange(EXCHANGE_AUTH_ATTEMPTED, msg, routingKey)
}

const publishToAPICallResponseExchange = (msg, routingKey) => {
  return publishToExchange(EXCHANGE_VALIDATE_API_CALL_RES, msg, routingKey)
}

const ack = (message, allUpTo = false) => {
  rabbitChannel.ack(message, allUpTo)
}

const nack = (message, multiple = false, requeue = false) => {
  rabbitChannel.nack(message, multiple, requeue)
}

module.exports = {
  openConnection,
  registerValidateAPICallConsumer,
  registerAuthVerificationConsumer,
  publishToAuthAttemptedExchange,
  publishToAPICallResponseExchange,
  ack,
  nack
}
