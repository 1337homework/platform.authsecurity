const SecurityPathEntity = require('../models/securityPathEntity')

const getPathEntity = async (path) => {
  return await SecurityPathEntity
    .query()
    .where('path', path)
    .first()
}

module.exports = {
  getPathEntity
}
