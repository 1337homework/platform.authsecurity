const moment = require('moment')
const NonceEntity = require('../models/nonceEntity')

const createNonceEntity = async (pathData, nonce, path) => {
  return await NonceEntity
    .query()
    .insert({
      nonce,
      path,
      requestedScopes: pathData.scopes,
      createdAt: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
    })
}

const getNonceEntity = async (nonce) => {
  return await NonceEntity
    .query()
    .where('nonce', nonce)
    .first()
}

module.exports = {
  createNonceEntity,
  getNonceEntity
}
